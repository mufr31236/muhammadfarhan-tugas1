import { Button, Image, Pressable, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <View style = {style.outerContainer}>
          <View style = {style.container}>
            <View style = {style.title}>
              <Text style = {style.titleText}>Digital Approval</Text>
            </View>

            <Image
              style = {style.icon}
              source={require('./assets/icon.png')}
            />

            <TextInput
              style= {style.textInput}
              placeholder='Alamat Email'
            />

            <TextInput
              style= {style.textInput}
              placeholder='Password'
            />

            <Text style= {style.resetText}>Reset Password</Text>

            <TouchableOpacity style= {style.loginButton}>
              <Text style= {style.textLogin}>LOGIN</Text>
            </TouchableOpacity>
            
          </View>
      </View>
    )
  }
}

const style = StyleSheet.create({
  outerContainer: {
    backgroundColor: '#F7F7F7',
    flex: 1
  },

  container : {
    marginVertical: 150,
    marginHorizontal: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 25
  },

  title: {
    borderRadius: 50,
    backgroundColor: '#002558',
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: -20
  },

  titleText:{
    color: 'white',
    fontWeight: 'bold'
  },

  icon: {
    height: 50,
    width: 120,
    marginVertical: 30
  },

  textInput:{
    borderWidth: 1,
    borderColor: '#002558',
    marginVertical: 10,
    marginHorizontal: 30,
    paddingHorizontal: 20,
    alignSelf: 'stretch',
  },

  resetText: {
    marginHorizontal: 30,
    textAlign: 'right',
    color: '#287AE5',
    fontStyle: 'italic',
    alignSelf: 'flex-end'
  },

  loginButton: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor:'#287AE5',
    alignSelf: 'stretch',
    borderRadius: 25,
    alignContent: 'center'
  },

  textLogin: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  }
})